:set tabpagemax=100
:set expandtab
:set tabstop=4
:set shiftwidth=4
let c_space_errors = 1
:syntax on
:nmap <F8> :tabp<CR>
:nmap <F9> :tabn<CR>
:filetype on
:filetype plugin indent on
:set tags=./tags;/

" Map leader to space instead of \
nnoremap <SPACE> <Nop>
let mapleader=" "

" Highlight column for aiding in keeping line length down
:nmap <leader>cc :set colorcolumn=81<CR>
:nmap <leader>co :set colorcolumn=""<CR>

" Allows for fast navigation using Ctrl
:nmap <C-j> 10j
:nmap <C-k> 10k
:nmap <C-l> 10l
:nmap <C-h> 10h
:vmap <C-j> 10j
:vmap <C-k> 10k
:vmap <C-l> 10l
:vmap <C-h> 10h

" remap quit to bufferdestroy
function! NrBufs()
    let i = bufnr('$')
    let j = 0
    while i >= 1
        if buflisted(i)
            let j+=1
        endif
        let i-=1
    endwhile
    return j
endfunction

function! NotLastBuf()
    let num_bufs = NrBufs()
    return num_bufs > 1
endfunction

"nnoremap <> :q<CR> LastBuf() ? ':q<CR>':':bd<CR>'
cnoreabbrev <expr> q NotLastBuf() && getcmdtype() == ":" && getcmdline() == 'q' ? 'bd' : 'q'
cnoreabbrev <expr> wq NotLastBuf() && getcmdtype() == ":" && getcmdline() == 'wq' ? 'w\|bd' : 'wq'
" end quit remap

" Shortcut to resource ~/.vimrc
nmap <leader>vs :source ~/.vimrc<CR>

" Uncomment the following to have Vim jump to the last position
" " reopening a file
if has("autocmd")
    au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Plugins
call plug#begin('~/.vim/plugged')
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'jlanzarotta/bufexplorer'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-tbone'
Plug 'airblade/vim-gitgutter'
Plug 'flazz/vim-colorschemes'
Plug 'vivien/vim-linux-coding-style'
Plug 'rust-lang/rust.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'neoclide/coc-json'
Plug 'neoclide/coc-tsserver'
Plug 'neoclide/coc-html'
Plug 'neoclide/coc-css'
Plug 'neoclide/coc-rls'
Plug 'neoclide/coc-yaml'
Plug 'neoclide/coc-python'
Plug 'neoclide/coc-git'
Plug 'fannheyward/coc-xml'
Plug 'mhinz/vim-grepper'
"Plug 'SirVer/ultisnips'
Plug 'mertzt89/grepper-nerdtree'
"Plug 'ryanoasis/vim-devicons'
"Plug 'ryanoasis/nerd-fonts'
call plug#end()

" CODING STYLES

" Linux coding style
let g:linuxsty_patterns = [ "projects/kernel-c4p",
                          \ "projects/kernel",
                          \ "projects/uboot-c4p",
                          \ "projects/uboot",
                          \ "projects/u-boot",
                          \ "projects/mcp25xxfd",
                          \ "programming/git/linux",
                          \ "programming/git/switchroot",
                          \ "ovrsource/Firmware/projects/merlot",
                          \ "mos/kernel",
                          \ ]
nnoremap <silent> <leader>cl :e<CR>:LinuxCodingStyle<cr>

function! MyHighlighting()
    highlight default link LinuxError ErrorMsg

    syn match LinuxError / \+\ze\t/     " spaces before tab
    syn match LinuxError /\%>100v[^()\{\}\[\]<>]\+/ " virtual column 101 and more

    " Highlight trailing whitespace, unless we're in insert mode and the
    " cursor's placed right after the whitespace. This prevents us from having
    " to put up with whitespace being highlighted in the middle of typing
    " something
    autocmd InsertEnter * match LinuxError /\s\+\%#\@<!$/
    autocmd InsertLeave * match LinuxError /\s\+$/
endfunction

" Garmin coding style
nnoremap <silent> <leader>cg :e<CR>
                            \:set cino=^-s,{1s<CR>
                            \:set cinw=if,else,while,do,for,switch<CR>
                            \:set cink=0{,0},0),:0#,!^F,o,O,e<CR>
                            \:set expandtab<CR>
                            \:set tabstop=4<CR>
                            \:set shiftwidth=4<CR>
                            \:set textwidth=100<CR>
                            \:call MyHighlighting()<CR>

" My preferred coding style
nnoremap <silent> <leader>cd :e<CR>
                            \:set cino=<CR>
                            \:set cinw=if,else,while,do,for,switch<CR>
                            \:set cink=0{,0},0),:0#,!^F,o,O,e<CR>
                            \:set expandtab<CR>
                            \:set tabstop=4<CR>
                            \:set shiftwidth=4<CR>
                            \:set textwidth=100<CR>
                            \:call MyHighlighting()<CR>

" FB C++ coding style
nnoremap <silent> <leader>cf :e<CR>
                            \:set cino=<CR>
                            \:set cinw=if,else,while,do,for,switch<CR>
                            \:set cink=0{,0},0),:0#,!^F,o,O,e<CR>
                            \:set expandtab<CR>
                            \:set tabstop=2<CR>
                            \:set shiftwidth=2<CR>
                            \:set textwidth=80<CR>
                            \:call MyHighlighting()<CR>

" tmux buffer yank/put
vnoremap <silent> <leader>ty :Tyank<CR>
nnoremap <silent> <leader>tp :Tput<CR>
nnoremap <silent> <leader>tP :-1Tput<CR>

" Set airline theme
let g:airline_theme='dark'
let g:airline#extensions#tabline#enabled=1

" Only show names of buffers/tabs, not full paths
let g:airline#extensions#tabline#fnamemod=':t'

" GitGutter
" Prevent git gutter from mapping keys (its mapping slows down Leader+h)
let g:gitgutter_map_keys=0
nnoremap <leader>gb :Gblame<cr>

" NERDTree
let NERDTreeShowHidden=1
nmap <leader>o :NERDTreeToggle<CR>
nmap <leader>O :NERDTreeFind<CR>

" Ctrlp
let g:ctrlp_max_files=0
let g:ctrlp_max_depth=40
" Use fd for ctrlp.
if executable('fd')
        let g:ctrlp_user_command = 'fd -I -c never "" %s'
            let g:ctrlp_use_caching = 0
        endif
:nmap <C-b> :CtrlPBuffer<CR>

" Set colorscheme
colorscheme ron
:set bg=dark

" Set wscript file syntax to python
au BufNewFile,BufRead,BufReadPost wscript set syntax=python
" Set local.conf syntax to sh
au BufNewFile,BufRead,BufReadPost local.conf set syntax=sh
au BufNewFile,BufRead,BufReadPost *.bb set syntax=sh
au BufNewFile,BufRead,BufReadPost *.bbappend set syntax=sh
au BufNewFile,BufRead,BufReadPost *.bbclass set syntax=sh

" use UTF-8 encoding
set encoding=UTF-8

call coc#config('coc.preferences', {
    \ 'timeout': 1000,
    \  "rootPatterns": ["compile_commands.json"]
    \})
call coc#config('languageserver', {
    \ 'ccls': {
    \   "command": "ccls",
    \   "trace.server": "verbose",
    \   "filetypes": ["c", "cpp", "objc", "objcpp"],
    \   "rootPatterns": [".ccls", "compile_commands.json", ".vim/", ".git/", ".hg/"],
    \   "initializationOptions": {
    \       "cache": {
    \           "directory": ".ccls-cache"
    \       }
    \   }
    \ }
    \})
call coc#config('coc.source.file.enable', 'true')

" coc bindings
nmap <silent> <leader>[ <Plug>(coc-definition)
nmap <silent> <leader>] <Plug>(coc-references)
nn <silent> K :call CocActionAsync('doHover')<cr>

" use <tab> for trigger completion and navigate to the next complete item
"function! s:check_back_space() abort
"  let col = col('.') - 1
"  return !col || getline('.')[col - 1]  =~ '\s'
"endfunction
"
"inoremap <silent><expr> <Tab>
"      \ pumvisible() ? "\<C-n>" :
"      \ <SID>check_back_space() ? "\<Tab>" :
"      \ coc#refresh()

"grepper
runtime plugin/grepper.vim
let g:grepper.tools =
    \ ['rg', 'git', 'ag', 'ack', 'ack-grep', 'grep', 'findstr', 'pt', 'sift']
let g:grepper.rg.grepprg .= ' --no-ignore'
let g:grepper.operator.tools =
    \ ['rg', 'git', 'ag', 'ack', 'ack-grep', 'grep', 'findstr', 'pt', 'sift']
let g:grepper.operator.rg.grepprg .= ' --no-ignore'
nnoremap <leader>gg :Grepper<cr>
xmap <leader>gs <plug>(GrepperOperator)






" BELOW ONLY PUT STUFF FROM OTHER PEOPLE'S VIMRCs

" Jesse's minimal .vimrc

" BASIC VIM OPTIONS
" This is not an exhaustive list of options it is simply the
" ones that I like to use. I have commented out the ones that
" I feel would be user preference

" Compatibility options
set nocompatible " No (vi) compatibility mode. Necessary to use vim features
"set mouse=ar     " Enable mouse support
set backspace=2  " Fixes backspace cross-platform

set autoindent   " Enable auto-indentation
set hlsearch     " Highlight searches
set number       " Enable line numbers
set laststatus=2 "Always show status line

" Changes how tab completion on the command line works.
" This is linux style where it completes until there is
" an ambiguity then lists the possibilities
set wildmenu
set wildmode=list:longest

" Uncomment if you want to disable soft wrapping
set nowrap

" Update at 100ms so gitgutter works better
set updatetime=100

" Uncomment if you want virtual spaces
"set virtualedit=all

" tab/spaces configs
"set expandtab " use tabs instead of spaces
"set tabstop=8
"set softtabstop=4
"set shiftround
"set shiftwidth=4
"set noexpandtab

" Whitespace visualization
"if has('nvim')
"	set listchars=tab:>-,space:∙
"else
"	set listchars=tab:>- " Vim can only do tabs
"endif

"set list

"set lazyredraw " Lazy redraw is useful if running complex macros

" Needed to fix cursor drawing issues in terminals
if !has('gui')
	set guicursor=
endif

" C indentation options (Garmin Style)
" set cino=^-s,{1s
" set cinw=if,else,while,do,for,switch
" set cink=0{,0},0),:0#,!^F,o,O,e

" Allow modified buffers to be hidden
set hidden
"

" CUSTOM KEY BINDINGS
" Disable arrow keys in normal mode
nnoremap <DOWN>  <NOP>
nnoremap <UP>    <NOP>
nnoremap <LEFT>  <NOP>
nnoremap <RIGHT> <NOP>

" Map arrow keys to window resizing
nmap <UP>    <C-W>+
nmap <DOWN>  <C-W>-
nmap <LEFT>  <C-W><
nmap <RIGHT> <C-W>>

" Add buffer navigation commands
nmap <leader>l :bnext<CR>
nmap <leader>h :bprevious<CR>
"nmap <leader>bw :bp <BAR> bd #<CR>
nmap <leader>bb :ToggleBufExplorer<CR>
nmap <leader><space> :ToggleBufExplorer<CR>

" Add window navigation commands
"nmap <leader>wl :wincmd l<CR>
"nmap <leader>wh :wincmd h<CR>
"nmap <leader>wk :wincmd k<CR>
"nmap <leader>wj :wincmd j<CR>
nnoremap <leader>w <C-w>

" <Leader>+o to open NERDTree to the current buffer's directory
"nmap <leader>o :NERDTreeToggle %<CR>

" Miscellaneous
" If there is a .project.vim in the pwd when vim is
" started then source it. This is useful for setting
" project specific indentation options, spaces vs. tabs
" etc.
if filereadable(".project.vim")
	source .project.vim
endif
"

" Auto relative line numbers
"set number relativenumber

"augroup numbertoggle
"	autocmd!
"	autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
"	autocmd BufLeave,FocusLost,InsertEnter * set norelativenumber
"augroup END
"

" Color Scheme
"colorscheme gruvbox
"let g:gruvbox_contrast_dark="hard"
"set bg=dark
"

" vim: set foldmethod=marker:
