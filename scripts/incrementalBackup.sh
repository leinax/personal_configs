#!/bin/bash

if [ $# -lt 1 ] ; then
    echo "Must specify arguments"
    exit 1
fi

opts=$(getopt -o "" -l "source:,destination:,dest-host:,dest-user:,dest-port:,exclude:,keep:,prefix:,lockfile-name:,check-mount-point:,fake-super-local,fake-super-remote" -n "incrementalBackup" -- "$@")

if test $? != 0 ; then
    echo "Failed parsing options" >&2
    exit 1
fi

echo "opts = $opts"
eval set -- "$opts"

source_dir=""
dest_dir=""
dest_ssh_host=""
dest_ssh_user=""
dest_ssh_port="22"
exclude_file=""
include_file=""
keep_days=""
date=$(date "+%Y_%m_%d-T_%H_%M_%S")
prefix="back"
lockfile_name="incrementalBackup.lock"
check_mount_point=""
fake_super_local=""
fake_super_remote=""

while true; do
    case "$1" in
        --source)
            source_dir="$2"
            shift 2
            ;;
        --destination)
            dest_dir="$2"
            shift 2
            ;;
        --dest-host)
            dest_ssh_host="$2"
            shift 2
            ;;
        --dest-user)
            dest_ssh_user="$2"
            shift 2
            ;;
        --dest-port)
            dest_ssh_port="$2"
            shift 2
            ;;
        --include)
            include_file="$2"
            shift 2
            ;;
        --exclude)
            exclude_file="$2"
            shift 2
            ;;
        --keep)
            keep_days="$2"
            shift 2
            ;;
        --prefix)
            prefix="$2"
            shift 2
            ;;
        --lockfile-name)
            lockfile_name="$2"
            shift 2
            ;;
        --check-mount-point)
            check_mount_point="$2"
            shift 2
            ;;
        --fake-super-local)
            fake_super_local=1
            shift 1
            ;;
        --fake-super-remote)
            fake_super_remote=1
            shift 1
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Internal error!"
            exit 1
            ;;
    esac
done

#only one instance can run at a time
LOCKFILE_PATH="/tmp/${lockfile_name}"
lockfile -r0 "$LOCKFILE_PATH"
if [ "$?" -ne 0 ] ; then
    echo "script instance already running"
    exit 1
fi

function egress {
    echo "removing lockfile"
    rm -f "$LOCKFILE_PATH"
}
trap egress EXIT

ssh_cmd_prefix=""
if [ "$dest_ssh_user" != "" ] ; then
    ssh_cmd_prefix="ssh -p $dest_ssh_port ${dest_ssh_user}@"
elif [ "$dest_ssh_host" != "" ] ; then
    ssh_cmd_prefix="ssh -p $dest_ssh_port "
fi

if [ "$dest_ssh_host" != "" ] ; then
    ssh_cmd_prefix="${ssh_cmd_prefix}${dest_ssh_host} \""
fi

ssh_cmd_postfix=""
if [ "$dest_ssh_host" != "" ] ; then
    ssh_cmd_postfix="\""
fi

backup_filename="$prefix-$date"

# determine if the prior attempt was interrupted
# first grab the most recent filename matching our prefix
resuming_prior_backup_attempt=""
most_recent_file_cmd="${ssh_cmd_prefix}cd \"$dest_dir\" && ls -t | grep \"$prefix-\" | head -n1"
most_recent_file_cmd="${most_recent_file_cmd}${ssh_cmd_postfix}"
echo "$most_recent_file_cmd"
most_recent_prefixed=$(eval "$most_recent_file_cmd")
echo "most_recent_prefixed=${most_recent_prefixed}"

# grab most recent filename in the directory (should be current symlink if past attempt succeeded)
most_recent_file_cmd="${ssh_cmd_prefix}cd \"$dest_dir\" && ls -t | head -n1"
most_recent_file_cmd="${most_recent_file_cmd}${ssh_cmd_postfix}"
echo "$most_recent_file_cmd"
most_recent_overall=$(eval "$most_recent_file_cmd")
echo "most_recent_overall=${most_recent_overall}"

# if they match, it means most recent attempt failed, resume it instead of starting over
if [ "${most_recent_prefixed}" == "${most_recent_overall}" ] ; then
    backup_filename="$most_recent_prefixed"
    echo "previous in-progress backup detected, resuming \"${backup_filename}\""

    # touch the directory to make sure it's last modified date is now (prevent accidental auto removal)
    most_recent_file_cmd="${ssh_cmd_prefix}touch \"$dest_dir/$backup_filename\""
    most_recent_file_cmd="${most_recent_file_cmd}${ssh_cmd_postfix}"
    echo "$most_recent_file_cmd"
    eval "$most_recent_file_cmd"

    resuming_prior_backup_attempt="1"
else
    echo "no previous in-progress backup detected, using new directory \"${backup_filename}\""
fi

if [ "$source_dir" = "" ] ; then
    echo "Error: must provide source directory"
fi
if [ "$dest_dir" = "" ] ; then
    echo "Error: must provide destination directory"
fi

if [ "$check_mount_point" != "" ] ; then
    echo "Checking that there is a fs mounted at \"$check_mount_point\""
    mountpoint "$check_mount_point"
    if [ $? -ne 0 ]; then
        echo "Not mounted! Exiting..."
        exit 1
    fi
fi

current_path="$dest_dir/current"
rsync_opts="-aAX --verbose"

if [ "$fake_super_local" -eq 1 ]; then
    rsync_opts="$rsync_opts --fake-super"
fi
if [ "$fake_super_remote" -eq 1 ]; then
    rsync_opts="$rsync_opts -M--fake-super"
fi

rsync_opts="$rsync_opts --link-dest=../../current/backup"

if [ "$include_file" != "" ] ; then
    rsync_opts="$rsync_opts --include-from $include_file"
fi

if [ "$exclude_file" != "" ] ; then
    rsync_opts="$rsync_opts --exclude-from $exclude_file"
fi

rsync_ssh=""
if [ "$dest_ssh_host" != "" ] ; then
    rsync_ssh="ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p ${dest_ssh_port}"
fi

rsync_opts="$rsync_opts $source_dir "

if [ "$dest_ssh_user" != "" ] ; then
    rsync_opts="${rsync_opts}${dest_ssh_user}@"
fi

if [ "$dest_ssh_host" != "" ] ; then
    rsync_opts="${rsync_opts}${dest_ssh_host}:"
fi

rsync_opts="${rsync_opts}${dest_dir}/${backup_filename}/backup"
echo "rsync -e \"$rsync_ssh\" $rsync_opts"

if [ "$resuming_prior_backup_attempt" == "" ] ; then
    mkdir_cmd="${ssh_cmd_prefix}mkdir ${dest_dir}/${backup_filename}${ssh_cmd_postfix}"
    echo "$mkdir_cmd"
    eval "$mkdir_cmd"
fi

rsync -e "$rsync_ssh" $rsync_opts

if [ $? -eq 0 ] ; then
    current_cmd="${ssh_cmd_prefix}rm $dest_dir/current ; cd $dest_dir && ln -s ${backup_filename} current"

    if [ "$keep_days" != "" ] ; then
        current_cmd="$current_cmd && mkdir -p \"/tmp/empty_dir/\" && find \"$dest_dir/\" -maxdepth 1 -type d -name '$prefix-*' -mtime +$keep_days -exec rsync -a --delete \"/tmp/empty_dir/\" \"{}/\" \\; -exec rm -rf \"{}\" \\;"
    fi

    current_cmd="${current_cmd}${ssh_cmd_postfix}"

    echo "$current_cmd"

    eval "$current_cmd"
fi

