#!/usr/bin/env python3

import evdev
import os
import time
import multiprocessing
import pyudev

auto_mute_flag = "/tmp/sway_auto_mute_unmute"
keyboard_names = [
        "AT Translated Set 2 keyboard",
        "daskeyboard",
        "SONIX USB DEVICE",
]

def udev_listener():
    print("starting udev listener")
    context = pyudev.Context()
    monitor = pyudev.Monitor.from_netlink(context)
    monitor.filter_by(subsystem='input')

    for device in iter(monitor.poll, None):
        print(device.action, device)
        if device.action == "add" or device.action == "remove":
            print(device.action, "detected for", device)
            # return to kill this subprocess
            return

def input_looper(dev):
    print("starting input loop for dev=", dev.name)
    while True:
        for event in dev.read_loop():
            if event.type == evdev.ecodes.EV_KEY and event.code == evdev.ecodes.ecodes['KEY_LEFTALT']:
                if event.value == 1:
                    os.system('sudo -u daniel DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus dbus-send --session --type=method_call --dest=net.sourceforge.mumble.mumble / net.sourceforge.mumble.Mumble.startTalking')
                    if os.path.exists(auto_mute_flag):
                        os.system('sudo -u daniel pactl --server=\"unix:/run/user/1000/pulse/native\" set-source-mute @DEFAULT_SOURCE@ false')
                elif event.value == 0:
                    os.system('sudo -u daniel DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus dbus-send --session --type=method_call --dest=net.sourceforge.mumble.mumble / net.sourceforge.mumble.Mumble.stopTalking')
                    if os.path.exists(auto_mute_flag):
                        os.system('sudo -u daniel pactl --server=\"unix:/run/user/1000/pulse/native\" set-source-mute @DEFAULT_SOURCE@ true')

while True:
    try:
        keyboard_devs = []
        devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
        for device in devices:
            print(device.path, device.name, device.phys)
            for name in keyboard_names:
                if device.name == name:
                    print("adding ", device.name)
                    keyboard_devs.append(device)
        print("num keyboard devs =", len(keyboard_devs))
        if len(keyboard_devs) == 0:
            time.sleep(1)
            continue

        looper_processes = [] 
        print("creating processes")
        looper_processes.append(multiprocessing.Process(target=udev_listener))
        for keyboard_dev in keyboard_devs:
            looper_processes.append(multiprocessing.Process(target=input_looper, args=(keyboard_dev,)))
        print("starting processes")
        for p in looper_processes:
            p.start()
        print("joining processes")
        while True:
            time.sleep(1)
            if len(multiprocessing.active_children()) != len(looper_processes):
                print("subprocess died!")
                for p in looper_processes:
                    p.terminate()
                time.sleep(1)
                break
    except:
        print("exception occurred")
        time.sleep(1)
